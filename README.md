# Imago2
Second iteration of the imago discord bot


## NOTICE: Imago2 is not longer being maintained or developed
No additional features will be added and no bugs will be fixed.

My instance of the bot, which you can [add here](https://discord.com/api/oauth2/authorize?client_id=731258805685583882&permissions=117760&scope=bot), will still be online and active.

I'm not sure if that is still the case whenever you're reading this, but it's still true as of 22/march/2022


### ok so does the code work or what
Yeah


### cool
ok but seriously the bot *works*, but its a mess and the code is barely readable. Lots of weird desing decisions on this one, but this project is pretty old by now so i guess ive just learned more things.

The major drawback is that its barely async, and will slow down significantly if for example more than 1 person is collecting their messages, or someone with a really big corpus is baking a model. This will basically halt handling any other command issued, so pretty awful.

ALSO the discord api is apparently changing a lot, so this will probably eventually fail. oh well. see yall on imago3 i guess.



# yeah alright but what is this
Imago is a bot that uses markov chains to generate new messages from your old discord messages!

If you know subredditsimulator, its pretty much that. You  ask the bot nicely to collect all your messages in a server, which it will then download & process into a markov chain. It can then "mimic" you, which means it will write a message it thinks you would write. Pretty neat!



Its an opt-in system so dont worry; if youre in a server with imago it wont randomly download all your messages until you specifically ask it to. Messages are also encrypted on disk with a passphrase decided by the bot host (the person that hosts the discord bot, which is usually me)


Some more on encryption: if you're not using my instance of this bot (like maybe your friend spun up an instance or something) you should ask them if they've enabled secure mode. Because its possible to disable the encryption (sort of). Which would be bad of course. just ask them if its on lol.

I tried to figure out a way to have some unique encryption for every user, but there's no real solid way to do that over discord without making it very annoying and complex to use, and you have to fully trust your instance's host anyways, since they could just run a modified version and steal your data anyways. Cryptography is hard dude.
