import random
from imago_config import config

import uwuify
from cowpy import cow
from PyDictionary import PyDictionary
vanDale = PyDictionary()

gspace = ' '

def uwu(sentence):
	flags = uwuify.SMILEY | uwuify.YU
	end = ''
	if not '!' in sentence and not '?' in sentence and not '!' in sentence:
		end = random.choice(['!','?','.'])
	return uwuify.uwu(sentence+end,flags=flags)

def cool(sentence):
	return sentence + ' B'+')'*random.randint(1, 15)

def thesaurus(sentence):
	words = sentence.split(' ')
	out = []
	for item in words:
		try:
			out.append(random.choice(vanDale.synonym(item)))
		except:
			out.append(item)
	return ' '.join(out)

def censor(sentence):
	words = sentence.split(' ')
	censorindex = random.randint(0, len(words)-1)
	word = words[censorindex]
	thingo = '\\*'
	word = f'{word[0]}{thingo*(len(word)-2)}{word[-1]}'
	words[censorindex] = word
	return ' '.join(words)

def scp(sentence):
	words = sentence.split(' ')
	index = random.randint(0, len(words)-1)
	words[index] = random.choice([f'[DATA{gspace}EXPUNGED]',str('█'*len(words[index])),'[REDACTED]'])
	x = random.choice(['**',''])
	words[index] = x+words[index]+x
	return ' '.join(words)


def cowsay(sentence):
	start='```\n'
	end='\n```'
	fout = sentence.replace('`','')
	out = start+cow.Cowacter().milk(fout)+end
	return out

def random_(sentence):
	return random.choice(list(lookup.values()))(sentence)

def aesthetic(sentence):
	out = ' '.join(list(sentence))
	print(out)
	out = out.replace('   ',f' {gspace}')
	out = italics(out)
	return out

def yell(sentence):
	return sentence.upper()+random.choice(['','','!','!!'])

def bold(sentence):
	return '**'+sentence+'**'

def italics(sentence):
	return '*'+sentence+'*'

def cursive(sentence):
	filterdict = {'a': '𝒶', 'b': '𝒷', 'c': '𝒸', 'd': '𝒹', 'e': '𝑒', 'f': '𝒻', 'g': '𝑔', 'h': '𝒽', 'i': '𝒾', 'j': '𝒿', 'k': '𝓀', 'l': '𝓁', 'm': '𝓂', 'n': '𝓃', 'o': '𝑜', 'p': '𝓅', 'q': '𝓆', 'r': '𝓇', 's': '𝓈', 't': '𝓉', 'u': '𝓊', 'v': '𝓋', 'w': '𝓌', 'x': '𝓍', 'y': '𝓎', 'z': '𝓏'}
	out = replacer(sentence,filterdict)
	return out

def whisper(sentence):
	filterdict = {'a': 'ᵃ', 'b': 'ᵇ', 'c': 'ᶜ', 'd': 'ᵈ', 'e': 'ᵉ', 'f': 'ᶠ', 'g': 'ᵍ', 'h': 'ʰ', 'i': 'ᶦ', 'j': 'ʲ', 'k': 'ᵏ', 'l': 'ˡ', 'm': 'ᵐ', 'n': 'ⁿ', 'o': 'ᵒ', 'p': 'ᵖ', 'q': 'ᑫ', 'r': 'ʳ', 's': 'ˢ', 't': 'ᵗ', 'u': 'ᵘ', 'v': 'ᵛ', 'w': 'ʷ', 'x': 'ˣ', 'y': 'ʸ', 'z': 'ᶻ'}
	out = replacer(sentence,filterdict)
	return out

def reverse(sentence):
	out = list(sentence)
	out.reverse()
	return ''.join(out)

def none(sentence):
	return sentence

def bin_(sentence):
	return ' '.join(format(ord(x), 'b') for x in sentence)

# def hex_(sentence):
# 	sentence.encode(encoding='utf_8')
# 	print(dir(sentence))
# 	return sentence.hex()

def rot13(sentence):
	a = {'a': 'n ', 'b': 'o ', 'c': 'p ', 'd': 'q ', 'e': 'r ', 'f': 's ', 'g': 't ', 'h': 'u ', 'i': 'v ', 'j': 'w ', 'k': 'x ', 'l': 'y ', 'm': 'z ', 'n': 'b ', 'o': 'c ', 'p': 'd ', 'q': 'e ', 'r': 'f ', 's': 'g ', 't': 'h ', 'u': 'i ', 'v': 'j ', 'w': 'k ', 'x': 'l ', 'y': 'm ', 'z': 'z', '?': '?', ' ': ' '}
	a.update({'A': 'N ', 'B': 'O ', 'C': 'P ', 'D': 'Q ', 'E': 'R ', 'F': 'S ', 'G': 'T ', 'H': 'U ', 'I': 'V ', 'J': 'W ', 'K': 'X ', 'L': 'Y ', 'M': 'Z ', 'N': 'B ', 'O': 'C ', 'P': 'D ', 'Q': 'E ', 'R': 'F ', 'S': 'G ', 'T': 'H ', 'U': 'I ', 'V': 'J ', 'W': 'K ', 'X': 'L ', 'Y': 'M ', 'Z': 'Z', '?': '?', ' ': ' '})
	return replacer(sentence, a)

def leet(sentence):
	return replacer(sentence, {'a': '4', 'b': '|3', 'c': '<', 'd': '|)', 'e': '3', 'f': '|"', 'g': '6', 'h': '|-|', 'i': '1', 'j': '/', 'k': '|<', 'l': '|', 'm': '/\\/\\', 'n': '|\\|', 'o': '0', 'p': '|7', 'q': '9', 'r': '|2', 's': '5', 't': '7', 'u': 'u', 'v': '\\/', 'w': '\\/\\/', 'x': '><', 'y': '\\`/', 'z': '2'})

def ace(sentence):
	return replacer(sentence, {'a':'ace','A':'ACE'},casing=True)

def anne(sentence):
	return sentence.replace('an','anne').replace('AN','ANNE').replace('An','Anne')
	# return sentence.replace('a','anne').replace('A','ANNE')

def garble(sentence):
	x = list(sentence)
	random.shuffle(x)
	return ''.join(x)

def shuffle(sentence):
	x = list(sentence.split(' '))
	random.shuffle(x)
	return ' '.join(x)

def wordsort(sentence):
	x = list(sentence.split(' '))
	x.sort()
	return ' '.join(x)

def lettersort(sentence):
	x = list(sentence)
	x.sort()
	return ''.join(x)

def lengthsort(sentence):
	x = list(sentence.split(' '))
	x.sort(key=lambda e: len(e))
	return ' '.join(x)

def sort_(sentence):
	x = sentence.split(' ')
	out = []
	for item in x:
		y = list(item)
		y.sort()
		y = ''.join(y)
		out.append(y)
	return ' '.join(out)

def emoji(sentence):
	emojidict = {'a': '🇦 ', 'b': '🇧 ', 'c': '🇨 ', 'd': '🇩 ', 'e': '🇪 ', 'f': '🇫 ', 'g': '🇬 ', 'h': '🇭 ', 'i': '🇮 ', 'j': '🇯 ', 'k': '🇰 ', 'l': '🇱 ', 'm': '🇲 ', 'n': '🇳 ', 'o': '🇴 ', 'p': '🇵 ', 'q': '🇶 ', 'r': '🇷 ', 's': '🇸 ', 't': '🇹 ', 'u': '🇺 ', 'v': '🇻 ', 'w': '🇼 ', 'x': '🇽 ', 'y': '🇾 ', 'z': '🇿 ', '?': '❔ ', ' ': '\u2003 '}
	out = replacer(sentence, emojidict)
	return out


def beans(sentence):
	return 'bonen '*sentence.count(' ')


lookup = {
	'none':none,
	'anne':anne,
	'emoji':emoji,
	'reverse':reverse,
	'cowsay':cowsay,
	'uwu':uwu,
	'yell':yell,
	'whisper':whisper,
	'bold':bold,
	'italics':italics,
	'cursive':cursive,
	# 'hex':hex_,
	'bin':bin_,
	# 'beans':beans,
	'sort':sort_,
	'lettersort':lettersort,
	'lengthsort':lengthsort,
	'wordsort':wordsort,
	'shuffle':shuffle,
	'aesthetic':aesthetic,
	'cool':cool,
	'random':random_,
	'thesaurus':thesaurus,
	'censor':censor,
	'scp':scp,
	'leet':leet,
	'ace':ace,
	'garble':garble,
	'rot13':rot13,
}



def replacer(sentence,filterdict,casing=False):
	if not casing:
		sentence = sentence.lower()

	table = sentence.maketrans(filterdict)
	return sentence.translate(table)
	# for item in sentence:
	# 	out += f'{filterdict.get(item,item)}'
	# return out