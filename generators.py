import time
import os
import random
from imago_config import config
from build_tools import getRawDataset

try:
	import markovify
	MARKOVIFY_LOADED = True
except Exception as e:
	MARKOVIFY_LOADED = False
	print('WARNING: markovify not loaded (is it installed?). some generators are unavailible')
	print(f'({e})')

try:
	from textgenrnn import textgenrnn
	TEXTGENRNN_LOADED = True
except Exception as e:
	TEXTGENRNN_LOADED = False
	print('WARNING: textgenrnn not loaded (is it installed?). some generators are unavailible')
	print(f'({e})')




'''
	generators.py

this houses all of the generator classes which can be called to get new generated text from a user's dataset
new generators are made by inheriting from currently the two template classes: markovTemplate and rnnTemplate
these templates are inhereted from the genericTemplate class, which has some common variables and functions


both the templates that inherit from genericTemplate and final generators inherited from one of the two templates have certain
functions they are expected to overwrite
the templates should overwrite the function names surrounded by single underscores (i.e. self._getStats_())
the generators should overwrite the function names surrounded by double underscores (i.e. self.__generate__())

this way generators (and future templates) all share a standardised way to get interacted with


im so sorry
'''


class genericTemplate():
	def __init__(self,dataset,type_,type_prefix,id_prefix,maxtries=10,author=None,version='1.0.0',blacklist=[],dataset_as_string=True):
		self.name = self.__class__.__name__
		self.type = type_
		self.id_prefix = id_prefix
		self.type_prefix = type_prefix
		self.debug_id = 'GEN-'+self.type_prefix+'-'+self.id_prefix + '['+str(self.__class__.__name__)+']:'
		self.maxtries = maxtries
		self.dataset = getRawDataset(dataset,dataset_as_string)

		self.bakedGenerator = None
		self.__author__= author
		self.__version__ = str(version)
		self.blacklist = blacklist + config['blacklist']

	def setDataset(self,newset):
		self.dataset = newset
		self.bakedGenerator = None

	def __generate__(self,bakedGenerator):
		raise NotImplemented(f'Generator {self.name} (of type {self.type}) has not imlpemented the __generate__() function')

	def __bake__(self):
		raise NotImplemented(f'Generator {self.name} (of type {self.type}) has not imlpemented the __bake__() function')

	def _getStats_(self):
		# this is where additional stats for specific types get returned
		return {}

	def getStats(self):
		out = {}
		out['name'] = self.name
		out['type'] = self.type
		out['baked'] = bool(self.bakedGenerator)
		out.update(self._getStats_())
		return out


	def generate(self):
		if self.bakedGenerator:
			for i in range(0,self.maxtries):
				out = self.__generate__(self.bakedGenerator)
				bad = False
				if out == None or out == '':
					bad = True
				else:
					for word in self.blacklist:
						if word in out:
							bad = True
				if not bad:
					return out
				else:
					print('bad!')
					continue
			raise Exception('Could not form proper sentence')
		else:
			self.bake()
			return self.generate()

	def _beforebake_(self):
		pass

	def bake(self):
		print(self.debug_id,'start bake')
		self._beforebake_()
		self.bakedGenerator = self.__bake__()
		self._afterbake_()
		print(self.debug_id,'finish bake')

	def _afterbake_(self):
		pass

class markovTemplate(genericTemplate):
	def __init__(self,dataset,id_prefix='non-reg',maxtries=100,author=None,version='1.0.0',blacklist=[]):
		super().__init__(dataset,'Markov chain','MKV',id_prefix,maxtries=maxtries,author=author,version=version,blacklist=blacklist,dataset_as_string=False)

		self.maxtries = maxtries
		self.statesize = '-'
		self.quality = '-'


	def _getStats_(self):
		out = {}
		out['quality'] = self.quality
		out['state size'] = self.statesize
		return out

	def __bake__(self):
		# this function must be overwritten by the child
		# must return a generic "generator" (confusing name i kno,ww,,) (in this case its a markovify generator object)
		self.quality = 0.0
		# basically in the case of markovify just do newlinetext or something and return it
		generator, self.statesize, self.quality = markovBaker(self.dataset)
		self.statesize = str(self.statesize)
		self.quality = str(self.quality)
		generator.compile(inplace = True)
		return generator

	def __generate__(self,bakedObj):
		# this function *can* be overwritten by the child, but doesnt have to be.
		# takes 1 argument
		# literally just call the function to generate text and return it, in the case of markovify
		return bakedObj.make_sentence(tries=config['markov']['max_tries'],max_overlap_ratio=config['markov']['max_overlap_ratio'])
		# print(f'error: {self.__class__.__name__} has not overwritten function __generate__()!!!')
		# raise NotImplemented('function __generate__() has not been implemented by this generator')


	def _afterbake_(self):
		self.quality = markov_testGenerator(self.bakedGenerator)



class rnnTemplate(genericTemplate):
	def __init__(self,dataset,id_prefix='non-reg',maxtries=100,author=None,version='1.0.0',blacklist=[],bakeoptions={},genoptions={},handle_saving=True):
		super().__init__(dataset,'Recurrent Neural Network','RNN',id_prefix,maxtries=maxtries,author=author,version=version,blacklist=blacklist,dataset_as_string=False)
		self.savename = f'{self.id_prefix}_{self.name}.hdf5'
		self.savepath = os.path.join(config['root'],'data',self.savename)
		self.handle_saving = handle_saving
		self.bakeoptions = bakeoptions
		self.genoptions = genoptions
		self.genoptions['return_as_list'] = True

	def _getStats_(self):
		return {}

	def __bake__(self):
		if self.handle_saving:
			textgen = textgenrnn()
			if os.path.isfile(self.savepath):
				textgen.load(self.savepath)
			else:
				textgen.train_on_texts(self.dataset, **self.bakeoptions)
		else:
			textgen = textgenrnn()
			textgen.train_on_texts(self.dataset, **self.bakeoptions)
		return textgen

	def _afterbake_(self):
		if self.handle_saving:
			self.bakedGenerator.save(self.savepath)

	def __generate__(self,bakedGenerator):
		return bakedGenerator.generate(**self.genoptions)[0]

# textgenrnn

class simplernn(rnnTemplate):
	def __init__(self,dataset,id_prefix,blacklist=[]):
		bakeoptions = {'num_epochs':1}
		genoptions = {'n':1}
		super().__init__(dataset,id_prefix,blacklist=blacklist,bakeoptions=bakeoptions,genoptions=genoptions,handle_saving=True)


# markovs

class wordsoup(markovTemplate):
	def __init__(self,dataset,id_prefix='non-reg',state_size=8):
		super().__init__(dataset,id_prefix,author='Anne')
		if type(self.dataset) == list:
			self.dataset = '\n'.join(self.dataset)
		self.dataset = self.dataset.replace(' ', '§')
		self.dataset = ' '.join(list(self.dataset))
		self.state_size = state_size

	def __bake__(self):
		a = markovify.NewlineText(self.dataset,state_size=self.state_size)
		a.compile(inplace = True)
		self.a = a
		# print(self.debug_id,'quality:',self.quality)
		return a

	def _afterbake_(self):
		self.statesize = str(self.state_size)
		self.quality = str(markov_testGenerator(self.a))
		del self.a


	def __generate__(self,bakedObj):
		try:
			out = bakedObj.make_sentence(test_output=False)
			out = out.replace(' ','').replace('§',' ')
			return out
		except:
			return None

class lettersoup(wordsoup):
	def __init__(self,dataset,id_prefix='non-reg'):
		super().__init__(dataset,id_prefix,state_size=2)
		# self.statesize=3


class default(markovTemplate):
	def __init__(self,dataset,id_prefix='non-reg'):
		super().__init__(dataset,id_prefix,author='Anne')


class random_(markovTemplate):
	def __init__(self,dataset,id_prefix):
		super().__init__(dataset, id_prefix)
		self.name = 'random'

	def __bake__(self):
		# for item in self.dataset.split('\n'):
		# 	newset.append(item.split(' '))
		return self.dataset

	def _afterbake_(self):
		self.quality = str(999)

	def __generate__(self,bakedset,lengthmax=10,lengthmin=1):
		# print(str(self.dataset)[:70])
		sentence = []
		for i in range(random.randint(lengthmin,lengthmax)):
			sentence.append(random.choice(bakedset))
		return ' '.join(sentence)


def markov_testGenerator(generator,total_samples=None,max_tries=None,max_overlap_ratio=None):
		if not total_samples:
			total_samples = config['markov']['test_samples']
		if not max_tries:
			max_tries = config['markov']['max_tries']
		if not max_overlap_ratio:
			max_overlap_ratio = config['markov']['max_overlap_ratio']
		goods = 0
		for i in range(0,total_samples):
			if generator.make_sentence(tries=max_tries,max_overlap_ratio=max_overlap_ratio) != None:
				goods += 1
		return goods/total_samples


def markovBaker(dataset,amount=1):
 	# default baker
 	winners = []
 	for state_size in range(config['markov']['max_statesize'],0,-1):
 		generator = markovify.NewlineText(dataset,state_size=state_size)
 		quality = markov_testGenerator(generator)
 		print(state_size,quality)
 		if quality >= config['markov']['minimum_quality']:
 			winners.append(generator)
 			if len(winners) == amount:
 				break
 		if state_size == 1:
 			winners.append(generator)

 	print(f'generated with state size {state_size} and quality {quality}')
 	return winners[0], state_size, quality





lookup = {'random':random_}

if MARKOVIFY_LOADED:
	lookup.update({'default':default,
					'wordsoup':wordsoup,
					'lettersoup':lettersoup,
		})

if TEXTGENRNN_LOADED:
	lookup.update({
		'rnn':simplernn,
		})
