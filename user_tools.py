#!/usr/bin/python3
import os
import time
import json
import random
from hashlib import sha512

import markovify

import imago_config
from build_tools import getRawDataset,collectMessages

def checkOrMake(files=None,folders=None,write='',mode='w'):
	if files:
		if type(files) != list:
			files = [files]
		for i in files:
			if not os.path.isfile(i):
				with open(i,mode) as f:
					f.write(write)
				# self.logger.debug('created file '+i)
	if folders:
		if type(folders) != list:
			folders = [folders]
		for i in folders:
			if not os.path.isdir(i):
				os.mkdir(i)
				# self.logger.debug('created dir '+i)
			# config = imago_config.config





class user_tools():
	def __init__(self,logger,crypt,generators,max_loaded_users=50,config=None):
		if config == None:
			self.config = imago_config.config
		self.logger = logger
		self.crypt = crypt
		self.generators = generators
		self.admingenerators = self.generators.copy()
		# del self.generators['rnn']

		self._idLookup_ = {}


		self.max_loaded_users = max_loaded_users

		self._users_ = {}
		try:
			self._userLookup_ = json.loads(self.crypt.decrypt_from_file(self.config['user_lookup']))
		except FileNotFoundError:
			self._userLookup_ = {'meta':{'data_tracker':0}}
			self.saveUserLookup()
			self.logger.info(f'User lookup initiated at {self.config["user_lookup"]}')

	def makeUser(self,user):
		userHASH = self.getSHA(user)
		entry = self._userLookup_[userHASH]
		# self.logger.debug('makeuser received: '+str(user)[:16])
		# self.logger.debug('and the hash we got: '+str(userHASH)[:16])
		# self.logger.debug(entry)
		datafile = entry['data_file']
		try:
			jsonraw = self.crypt.decrypt_from_file(datafile).decode()
		except FileNotFoundError:
			jsonraw = '{"administrator":false}'
		dataset = json.loads(jsonraw)
		isadmin = entry['administrator']
		if isadmin:
			generators = self.admingenerators
		else:
			generators = self.generators
		obj = userObj(userHASH,dataset,isadmin=isadmin,savefile=datafile,generatorTemplates=generators)
		return obj

	def userCount(self):
		return len(self._userLookup_)-1

	def makePath(self,user,override=False,
				default={'servers':{}}):
		userHASH = self.getSHA(user)
		self.logger.debug('makepath received: '+str(userHASH)[:16])
		savefile = os.path.join(self.config['data_folder'],f"{self.getUser('meta')['data_tracker']}.json")
		self.crypt.encrypt_to_file(savefile,json.dumps(default))
		self._userLookup_['meta']['data_tracker'] += 1
		return savefile

	def optIn(self,user):
		userHASH = self.getSHA(user)
		savefile = self.makePath(userHASH)
		# self.logger.debug('optin: '+str(user)+' '+str(userHASH)[:16],savefile)
		self._userLookup_[userHASH] = {
			'data_file':savefile,
			'opt_in_timestamp': time.time(),
			'administrator': False
		}
		self.logger.debug('giving to makeUser: '+str(userHASH)[:16])
		newuser = self.makeUser(userHASH)
		self._users_[userHASH] = newuser
		self.saveUser(userHASH,force=True)
		self.saveUserLookup()
		self.logger.info('New user opted in')
		return newuser

	def optOut(self,user):
		userHASH = self.getSHA(user)
		if self.isOptedIn(userHASH):
			try:
				os.remove(self._userLookup_[userHASH].get('data_file',False))
			except FileNotFoundError:
				pass
			except Exception as e:
				raise e
			del self._userLookup_[userHASH]
			del self._users_[userHASH]
			self.logger.info('User opted out')
			self.saveUserLookup()

	def isOptedIn(self,user):
		userHASH = self.getSHA(user)
		if self.isLoaded(userHASH):
			return True
		else:
			return userHASH in self._userLookup_

	def isAdmin(self,user):
		userHASH = self.getSHA(user)
		if self.isOptedIn(userHASH):
			if self.isLoaded(userHASH):
				return self.getUser(userHASH).isAdmin()
			else:
				return self._userLookup_.get(userHASH,{}).get('administrator',False)
		return False

	def isLoaded(self,user):
		userHASH = self.getSHA(user)
		return userHASH in self._users_


	def getUser(self,user):
		userHASH = self.getSHA(user)
		if userHASH == 'meta':
			return self._userLookup_['meta']
		x = self._users_.get(userHASH)
		if x == None:
			# self.logger.debug('NEW USER!!')
			if self.isOptedIn(userHASH):
				a = self.makeUser(user)
				self._users_[userHASH] = a
				if len(self._users_) > self.max_loaded_users:
					del self._users_[list(self._users_.keys()[0])]
				return a
			else:
				return None
		else:
			return x


	def getSHA(self,user):
		# self.logger.debug('getsha: '+str(user)[:16])
		def _getsha_(self,userID):
			# self.logger.debug('generating hash for '+userID)
			# returns either the stored sha512 of the userID or calculates a new one
			x = self._idLookup_.get(userID)
			if x == None:
				# userHASH = self.config['crypt'].encrypt(userID).decode()
				userHASH = sha512(userID.encode()).hexdigest()
				self._idLookup_[userID] = userHASH
				# self.logger.debug(f'generated hash: {str(userHASH)[:16]}...') ({type(userHASH)})')
				return userHASH
			else:
				# self.logger.debug(f'generating {x}? i guesss? this better not be meta or smth')
				return x
		# simple function to always securely get the sha512 from a valid string
		# "user" can either be the generated sha512 or a discord userID
		if user == 'meta':
			# self.logger.debug('returing meta')
			return user
		if type(user) == int:
			user = str(user)
		if len(user) == 128:
			# self.logger.debug('already hash!')
			return user
		else:
			return _getsha_(self,user)

	def saveUser(self,user,force=False):
		userHASH = self.getSHA(user)
		if self.isLoaded(userHASH) or force:
			they = self._users_.get(userHASH)
			if self._userLookup_[userHASH]['administrator'] != they.isAdmin():
				self._userLookup_[userHASH]['administrator'] = they.isAdmin()
			if they.hasupdated or force:
				self._userLookup_[userHASH]['administrator'] = they.isAdmin()
				savefile = they.savefile
				self.crypt.encrypt_to_file(savefile,they.to_json())
				they.hasupdated = False

	def saveUsers(self):
		for user in self._userLookup_:
			self.saveUser(user)

	def saveUserLookup(self):
		# self.logger.debug('user lookup:',self.user_lookup)
		self.crypt.encrypt_to_file(self.config['user_lookup'],json.dumps(self._userLookup_))

	def canExit(self):
		for user in self._users_.values():
			if user.isBuilding():
				return False
		return True

	def wrapUp(self):
		self.saveUsers()
		self.saveUserLookup()



class userObj():
	def __init__(self,userHASH,dataset={'servers':{}},generatorTemplates={},savefile=None,isbuilding=False,isadmin=False):
		self.data = dataset
		self.debug_id = userHASH[:6] + '-' + userHASH[-6:]
		self.raw = getRawDataset(self.data)
		self.isbuilding = isbuilding
		self.generatorTemplates = generatorTemplates
		self.generators = {}
		self.savefile = savefile
		self.userHASH = userHASH
		self.isadmin = isadmin
		self.hasupdated = False
		self.isbusy = False

		for item in self.generatorTemplates:
			self.addGenerator(item,self.generatorTemplates[item])

	def resetGuild(self,serverid):
		serverid = str(serverid)
		if serverid in self.data['servers']:
			# self.data['servers'][serverid]['raw'] = []
			# self.data['servers'][serverid]['latest_timestamp'] = 0
			# self.data['servers'][serverid]['elapsedtime'] = 0
			# self.data['servers'][serverid]['message_count'] = 0
			del self.data['servers'][serverid]
			self.hasupdated = True
			self.resetGenerators()
		else:
			raise Exception(f'No dataset for server {serverid}')


	def to_json(self):
		return json.dumps(self.to_dict())

	def to_dict(self):
		return self.data

	def setAdmin(self,state):
		self.isadmin = bool(state)

	def isBuilding(self):
		return self.isbuilding

	def isBusy(self):
		return self.isbusy

	def isAdmin(self):
		return self.isadmin

	def resetGenerators(self):
		self.generators = {}
		for item in self.generatorTemplates:
			self.generators[item] = self.generatorTemplates[item](self.data,id_prefix=self.debug_id)

	def addGenerator(self,name,newgen):
		a = newgen(self.data,id_prefix=self.debug_id)
		self.generatorTemplates[name] = newgen
		self.generators[name] = a

	def getData(self):
		return self.data

	def setData(self,dataset):
		self.data = dataset
		self.generators = {}
		self.hasupdated = True

	def getStats(self):
		out =  {
			'Server count':				len(self.data['servers'].keys()),
			'Message count':			len(self.raw),
			'Availible generators':		', '.join(list(self.generatorTemplates.keys())),
			'Generator stats': 	[]
		}
		for item in self.generators:
			out['Generator stats'].append(self.generators[item].getStats())
		return out
			# '\n'.join([a.stats for a in

	async def update(self,ctx):
		if not self.isbuilding:
			try:
				self.isbusy = True
				self.isbuilding = True
				server = str(ctx.guild.id)
				latest_timestamp = self.data['servers'].get(str(server),self.data['servers'].get(server,{})).get('latest_timestamp',0)
				print(latest_timestamp)
				server_data, latest_timestamp, elapsedtime = await collectMessages(ctx, untilTime=latest_timestamp)
				newdata = self.data['servers'].get(server,{})
				newdata['raw'] = newdata.get('raw',[]) + server_data
				newdata['latest_timestamp'] = latest_timestamp
				newdata['elapsedtime'] = elapsedtime
				newdata['message_count'] = len(server_data)
				self.data['servers'][server] = newdata
				self.raw = getRawDataset(self.data)
				self.generators = {}
				for item in self.generatorTemplates:
					self.addGenerator(item,self.generatorTemplates[item])
				self.hasupdated = True
				self.isbuilding = False
				self.isbusy = False
			except Exception as e:
				self.isbusy = False
				self.isbuilding = False
				raise e
		else:
			raise Exception('Please wait for the update to finish')

	def getGenerator(self,key):
		return self.generators.get(key)

	def generate(self,key):
		self.isbusy = True
		out = self.generators[key].generate()
		self.isbusy = False
		return out

	def hasServer(self,key):
		return key in self.data['servers']
