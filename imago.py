#!/usr/bin/python3
'''
	imago 2.2.0
	Markov discord bot with data encryption

	by (lis)anne/mocha
	gitlab.com/mocchapi/imago2
'''
import os
import sys
import time
import json
import random
import getpass
import logging
import secrets
import asyncio
import datetime
import importlib

import filters
import generators
from user_tools import user_tools
from cooldown_tools import cooldown_tools
from encryption_tools import encryption_tools

import discord
import markovify
from discord.ext import commands
# from discord.ext.commands import CommandNotFound

VERSION = '2.2.0'

bot = commands.Bot(['temporary','prefixes','these get overwritten with the config ones'], case_insensitive=True)





# /------------actual cmds------------\
@bot.command(name='reset',brief='Reset dataset for current server',help='Resets your dataset for the current server. This will void all previous data from this server. Dont use this unless you want to remove parts of mimics from this server specifically. To remove all your data entirely, use !im opt out')
async def reset(ctx):
	if await optCheck(ctx, 'reset'):
		userTools.getUser(ctx.author.id).resetGuild(ctx.guild.id)
		await ctx.send('Your data for this server has been deleted.')

@bot.command(aliases=['about'],brief='Information about the bot')
async def info(ctx):
	servercount = len(bot.guilds)
	usercount = userTools.userCount()
	uptime = list(str(datetime.datetime.now()-runtime).split('.'))[0]
	embed = discord.Embed(title='Imago discord bot',type='rich')
	embed.set_thumbnail(url=bot.user.avatar_url)
	# msglines = []
	embed.add_field(name='version',value=VERSION)
	embed.add_field(name='Author:',value='Anne (Mocha#2341)')
	# embed.add_field(name=' ',value='')
	embed.add_field(name=f'Server count:',value=servercount)
	embed.add_field(name=f'User count:',value=usercount)
	embed.add_field(name=f'Uptime:',value=uptime)
	embed.add_field(name='Source:',value='[Gitlab](https://gitlab.com/mocchapi/imago2)')
	if len(config.get('invite','')) > 1:
		embed.add_field(name=f'Bot invite:',value=f'[Click here]({config.get("invite","")})')
	await ctx.send(embed=embed)

	# msglines.append(f'Uptime:         {datetime.datetime.now()-runtime}')


@bot.command(brief='Get a bot invite link to add Imago to another server')
async def invite(ctx):
	if await cooldownTools.vhandle(ctx, 'invite'):
		a = config.get('invite')
		if a == '':
			await ctx.send(f'Sorry, this command has been disabled')
		else:
			await ctx.send(f'Here you go: {a}')

@bot.command(name='filter',brief='Filter text',help=f'Filter text.\nAvailible filters: {", ".join(list(filters.lookup.keys()))}')
async def filter_(ctx,*args):
	if not await cooldownTools.vhandle(ctx, 'filter'): return
	try:
		arg = args[0]
		sentence = ' '.join(args[1:])
		out = filters.lookup[arg](sentence)
		await sendMim(ctx, out)
	except KeyError:
		await ctx.send(f'No such filter: "{arg}". Availible filters: {", ".join(list(filters.lookup.keys()))}')


@bot.command(brief='Statistics about your corpus')
async def stats(ctx):
	if not await optCheck(ctx,'stats'): return
	stats = userTools.getUser(ctx.author.id).getStats()
	msglines = []
	space = 22
	for item in stats:
		if item != 'Generator stats':
			msglines.append(f'{item}: {" "*(space-len(item))}{stats[item]}')
		else:
			msglines.append('\nGenerator stats:')
			for thingo in stats[item]:
				for realstat in thingo:
					msglines.append(' '*4+f'{realstat[0].upper()+realstat[1:]}: {" "*(space-len(realstat)-4)}{thingo[realstat]}')
				msglines.append('')

	# print(msg_count)
	# print(generator.keys())
	newline = '\n'
	await ctx.send(f'''__Corpus stats for {ctx.author}:__
```
{newline.join(msglines)}
```''')

@bot.command(brief='Update your corpus with new messages')
async def update(ctx,*args):
	# print(args)
	if not await optCheck(ctx,'update'): return
	try:
		message = 'Updating corpus, '
		user = userTools.getUser(ctx.author.id)
		if user.hasServer(ctx.guild.id):
			message += 'this can take a long time, go grab a snack :3'
		else:
			message += 'this shouldn\'t take that long :3'
		await ctx.send(message)
		await user.update(ctx)
		# await buildTools.build(ctx,limit=None)
		await ctx.send(f'Corpus updated for <@{ctx.author.id}>')
	except BaseException as e:
		logger.error(e)
		await ctx.send(f'Error: {e}')
		raise e

@bot.command(aliases=['m'],name='mimic',brief='mimic with optionally different generators and filters',help=f'Use either on its own, or with !im mimic <generator> <filter>\nAvailible generators: {", ".join(list(generators.lookup.keys()))}\nAvailible filters: {", ".join(list(filters.lookup.keys()))}')
async def mimic(ctx,*args):
	if not await optCheck(ctx,'mimic'): return
	if len(args) > 0:
		arg = args[0]
		if len(args) > 1:
			filteroni = args[1:]
		else:
			filteroni = ['none']
	else:
		arg = 'default'
		filteroni = ['none']
	if arg in generators.lookup.keys():
		async with ctx.typing():
			out = userTools.getUser(ctx.author.id).generate(arg)
			try:
				for item in filteroni:
					out = filters.lookup[item](out)
				await sendMim(ctx, out)
			except KeyError:
				await ctx.send(f'No such filter: "{item}".\nAvailible filters: {", ".join(list(filters.lookup.keys()))}')
	else:
		await ctx.send(f'No such generator: "{arg}"\nAvailible generators: "{", ".join(list(generators.lookup.keys()))}"')

@bot.group(name='opt',brief='"in" or "out"',help='Opt in or out of imago')
async def opt(ctx):
	if ctx.invoked_subcommand is None:
		await ctx.send('No such command, do `opt in` or `opt out`')

@opt.command(name='in',brief='Opt in to imago and generate a corpus',help='Opts you in to imago and unlocks its features for you. This will automatically generate you a corpus. You can opt out at any time with !im opt out')
async def optIn(ctx):
	# if not await cooldownTools.vhandle(ctx, 'opt in'): return
	if not userTools.isOptedIn(ctx.author.id):
		user = userTools.optIn(ctx.author.id)
		await ctx.send('You are now opted in. Your messages will now be collected, which will take a long while. Go grab a snack, ill notify you when i\'m done :3')
		# user = userTools.getUser(ctx.author.id)
		await user.update(ctx)
		# # userTools.saveUser(ctx.author.id,force=True)
		# await ctx.send('You can now use the `!im mimic` command.\nIf you want to update your model to use new messages after a while, do `!im update` (updating is a lot faster than rebuilding)')
	else:
		await ctx.send('You are already opted in, use `!im opt out` to opt out & delete your data')

@opt.command(name='out',brief='Opt out of imago',help='Opt out of imago. This will delete all your user data ')
async def optOut(ctx):
	if userTools.isOptedIn(ctx.author.id):
		userTools.optOut(ctx.author.id)
		await ctx.send('You are now opted out. Your user data has been deleted')
	else:
		await ctx.send('You are not opted in')
# \-----------------------------------/



# /------------bot thingos------------\
@bot.event
async def on_command_error(ctx, error):
	if isinstance(error, commands.CommandNotFound):
		await ctx.send(f'No such command')
	else:
		await ctx.send(f'An error occured: `{error}`')
	raise error

@bot.event
async def on_ready():
	logger.info('Bot loaded')
	# print(dir(userTools))
	logger.info(f'User count: {userTools.userCount()}')

	activity_type = None
	if config['status'].get('type') == 'watching':
		activity_type = discord.ActivityType.watching
	elif config['status'].get('type') == 'playing':
		activity_type = discord.ActivityType.playing
	if activity_type and config['status'].get('type') and config['status'].get('text'):
		activity = discord.Activity(name=config['status']['text'], type=activity_type)
		await bot.change_presence(activity=activity)

@bot.event
async def on_message(ctx):
	if ctx.author.bot:
		# disallow bot input
		return

	for i in config['prefixes']:
		# check to only print commands
		if ctx.content.lower().startswith(i):
			if userTools.isOptedIn(ctx.author.id):
				userType = 'User '
				if userTools.isAdmin(ctx.author.id):
					userType = 'Admin'
			else:
				userType = 'None'
			print(f'[{ctx.guild}] <{userType}> (#{ctx.channel}) {ctx.author}: {ctx.content}')
	await bot.process_commands(ctx)
# \-----------------------------------/



# /------------nice checks------------\
async def optCheck(ctx,command):
	try:
		if userTools.getUser(ctx.author.id).isAdmin(): return True
	except:
		pass
	if not await cooldownTools.vhandle(ctx, command): return False
	if userTools.isOptedIn(ctx.author.id):
		return True
	else:
		await ctx.send('You must be opted in to use this command, do `!im opt in`')
		return False

async def adminCheck(ctx):
	try:
		if userTools.isAdmin(ctx.author.id):
			return True
		else:
			await ctx.send('You must be an Imago administrator to use this command')
	except:
		await ctx.send('You must be an Imago administrator to use this command')
# \-----------------------------------/



# /------------admin stuff------------\
@bot.command(name='filterReload',brief='Reload filters <Imago administrators only>')
async def reload_filters(ctx):
	if await adminCheck(ctx):
		sys.modules['filters'] = importlib.reload(sys.modules['filters'])

@bot.command(brief='Check if any corpi are being built <Imago administrators only>')
async def isbuilding(ctx):
	if await adminCheck(ctx):
		try:
			if userTools.canExit():
				await ctx.send('There are currently no corpi building')
			else:
				await ctx.send('There are currently one or more corpi building')
		except:
			pass


@bot.command(brief='Shut down bot <Imago administrators only>')
async def shutdown(ctx):
	if await adminCheck(ctx):
		s = shutdown_manager(ctx)
		await s.shutdown()

@bot.command(brief='Restart bot <Imago administrators only>')
async def restart(ctx):
	if await adminCheck(ctx):
		s = shutdown_manager(ctx)
		await s.restart()

@bot.command(name='eval',brief='Evaluate <Imago administrators only>',help='Evaluates a python bop')
async def eval_(ctx,*,args):
	if await adminCheck(ctx):
		# print(args,type(args))
		try:
			eval_out = str(eval(args))
			if len(eval_out) >= 2000:
				eval_out = eval_out[:1980]+'...'
		except Exception as e:
			eval_out = e
		# eval_out = str(eval_out)
		# print(eval_out)
		await ctx.send('Output: ```py\n'+str(eval_out)+'```',delete_after=45)



@bot.command(name='admin', brief='Make someone admin <Imago administrators only>')
async def admin(ctx,*,args):
	if await adminCheck(ctx):
		# print(dir(ctx.message))
		# print(dir(ctx.message.mentions[0]))
		for user in ctx.message.mentions:
			if user.bot:
				await ctx.send('Bots cannot become administrators')
				continue
			if user.id == ctx.author.id:
				await ctx.send('You are already an administrator')
				continue
			if userTools.isOptedIn(user.id):
				if userTools.isAdmin(user.id):
					await ctx.send(f'User {user} is already an administrator')
				else:
					userTools.getUser(user.id).setAdmin(True)
					await ctx.send(f'User {user} granted administrator permissions')
			else:
				await ctx.send(f'User {user} is not opted in')

# \-----------------------------------/








async def sendMim(ctx,message):
	try:
		await ctx.send(embed=fancyText(ctx.author,message))
	except:
		try:
			await ctx.send(f'{ctx.author}:\n> {message}')
		except:
			pass

def fancyText(author,message):
	embed = discord.Embed(description=message)
	embed.set_author(name=author.name,icon_url=author.avatar_url)
	return embed


class shutdown_manager():
	def __init__(self,ctx=None):
		self.ctx = ctx

	async def say(self,text):
		if self.ctx:
			await self.ctx.send(text)

	async def restart(self,force=False):
		if config['secure']:
			await self.say('Restarting is not possible in "secure" mode, please use `!im shutdown` and manually restart')
			logger.warning('Restarting is not possible in "secure" mode, please use `!im shutdown` and manually restart')
			return False
		if userTools.canExit() or force:
			logger.info('Restarting...')
			userTools.wrapUp()
			try:
				await self.say('Restarting...')
			except Exception as e:
				logger.error(e)
			await self.ctx.bot.close()
			os.execv(sys.argv[0], sys.argv)
		else:
			logger.info(f'Cant restart: there are still builds in progress')
			try:
				await self.say(f'Cant restart: there are still builds in progress')
			except Exception as e:
				logger.error(e)


	async def shutdown(self,force=False):
		if userTools.canExit() or force:
			logger.info('Shutting down...')
			userTools.wrapUp()
			try:
				await self.say('Shutting down')
			except Exception as e:
				logger.error(e)
			await self.ctx.bot.close()
			sys.exit()
		else:
			logger.info('Cant shut down: there are still builds in progress')
			try:
				await self.say('Cant shut down: there are still builds in progress')
			except Exception as e:
				logger.error(e)


def main():
	global logger
	global config
	global userTools
	global cooldownTools
	global crypt
	global runtime

	# logging.basicConfig(level=logging.INFO)
	logger = logging.getLogger(__file__)
	logger.setLevel(level=logging.DEBUG)
	fh = logging.StreamHandler()
	fh_formatter = logging.Formatter('%(asctime)s %(levelname)s %(lineno)d:%(filename)s(%(process)d) - %(message)s')
	fh.setFormatter(fh_formatter)
	logger.addHandler(fh)

	runtime = datetime.datetime.now()

	# vv this section loads the config file, and if it fails uses defaults as a fallback
	config_defaults = None
	try:
		import imago_config
		config = imago_config.config
		logger.debug(f'config file loaded')
	except Exception as e:
		config = config_defaults
		logger.error(f'config error: {e}')
		logger.warning(f'using default config')
	# ^^ wish there was a better character for up facing arrow


	# use password or not depending on config
	if config['secure']:
		logger.info('Running in secure mode')
		time.sleep(.02)
		print()
		print('"secure" is enabled in config, this means all user data is encrypted')
		print('Please provide the password you used the first time you ran this program')
		print('If this is the first time youve ran this program, please enter a password you think is secure')
		crypt = encryption_tools(getpass.getpass('Password: '))
		print()
	else:
		logger.info('Running in insecure mode')
		crypt = encryption_tools('hunter2')

	if not os.path.isdir(config['data_folder']):
		os.mkdir(config['data_folder'])
		logger.info('Created data folder')


	if os.path.isfile(config['token_file']):
		# load discord token from file
		try:
			token = crypt.decrypt_from_file(config['token_file']).decode()
		except:
			print('INCORRECT PASSWORD')
			print(f'If you want to reset password, please delete "{config["user_lookup"]}", "{config["data_folder"]}" and "{config["token_file"]}"')
			sys.exit()

	else:
		# get discord token from user and store in encrypted file
		print('Please insert your discord bot token')
		token = input('Discord token: ')
		crypt.encrypt_to_file(config['token_file'], token)


	# various parts of imago that make it work
	# userTools stores all user data
	userTools = user_tools(logger,crypt,generators.lookup)
	# cooldownTools handles user cooldowns/ratelimits
	cooldownTools = cooldown_tools(userTools)

	# launch discord
	bot.command_prefix = config['prefixes']
	bot.run(token)


if __name__ == '__main__':
	main()
