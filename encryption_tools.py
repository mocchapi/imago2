import secrets
from base64 import urlsafe_b64encode as b64e, urlsafe_b64decode as b64d

from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

class encryption_tools():
	# handles encryption and decryption
	# https://stackoverflow.com/a/55147077
	def __init__(self,password):
		self.password = password.encode()
		self.backend = default_backend()
		self.iterations = 100_000

	def _derive_key(self, salt: bytes, iterations: int = None) -> bytes:
		"""Derive a secret key from a given password and salt"""
		if iterations == None:
			iterations = self.iterations
		kdf = PBKDF2HMAC(
			algorithm=hashes.SHA256(), length=32, salt=salt,
			iterations=iterations, backend=self.backend)
		return b64e(kdf.derive(self.password))

	def encrypt(self, message: bytes, iterations: int = None,encoding = 'utf-8') -> bytes:
		if iterations == None:
			iterations = self.iterations
		if type(message) != bytes:
			message = bytes(message,encoding)
		salt = secrets.token_bytes(16)
		key = self._derive_key( salt, iterations)
		return b64e(
			b'%b%b%b' % (
				salt,
				iterations.to_bytes(4, 'big'),
				b64d(Fernet(key).encrypt(message)),
			)
		)

	def decrypt(self, token: bytes) -> bytes:
		if type(token) == str:
			token = bytes(token,'utf-8')
		decoded = b64d(token)
		salt, iter, token = decoded[:16], decoded[16:20], b64e(decoded[20:])
		iterations = int.from_bytes(iter, 'big')
		key = self._derive_key( salt, iterations)
		return Fernet(key).decrypt(token)

	# def encrypt(self,string:str):
	# 	pass
	# def decrypt(self,string:str):
	# 	pass

	def encrypt_to_file(self,file,text):
		with open(file,'wb') as f:
			f.write(self.encrypt(text))

	def decrypt_from_file(self,file):
		with open(file,'rb') as f:
			return self.decrypt(f.read())
