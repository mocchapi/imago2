import datetime
import asyncio
import time
import math

import imago_config

class cooldown_tools():
	def __init__(self,userTools,cooldowns=None,tracker={},config=None):
		self.userTools = userTools
		self.tracker = tracker
		self.blacklist = set()
		if config == None:
			config = imago_config.config
		self.config = config
		if cooldowns:
			self.cooldowns = cooldowns
		else:
			self.cooldowns = self.config.get('cooldowns')
		self.default = self.cooldowns.get('default',10)

	def check(self,user,command):
		userHASH = self.userTools.getSHA(user)
		if userHASH in self.blacklist:
			return False,-1
		x = self.tracker.get(userHASH)
		if x:
			y = x.get(command,0)
			if y:
				if y < time.time():
					return True,0
				else:
					return False,float(y - time.time())
			else:
				return True,0
		else:
			return True,0


	def set(self,user,command):
		userHASH = self.userTools.getSHA(user)
		if self.tracker.get(userHASH) == None:
			self.tracker[userHASH] = {}
		self.tracker[userHASH][command] = time.time() + self.cooldowns.get(command,self.default)


	def reset(self,user,comand):
		userHASH = self.userTools.getSHA(user)
		if self.tracker.get(userHASH) == None:
			self.tracker[userHASH] = {}
		self.tracker[userHASH][command] = 0


	def handle(self,user,command):
		allowed,timeleft = self.check(user,command)
		if allowed:
			self.set(user, command)
		return allowed,timeleft

	def blacklist(self,user,operation='add'):
		userHASH = self.userTools.getSHA(user)
		if operation == 'add':
			blacklist.add(userHASh)
		elif operaton == 'remove':
			blacklist.remove(userHASH)


	async def vhandle(self,ctx,command):
		userID = ctx.author.id
		allowed,timeleft = self.handle(userID, command)
		if not allowed:
			frmtTime = datetime.datetime.utcfromtimestamp(math.ceil(timeleft)).strftime("%H:%M:%S")
			try:
				await ctx.send(f'<@{userID}> please wait {frmtTime} before you use that command again')
			except:
				pass
			return False
		return True
