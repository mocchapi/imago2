import os
root = os.path.dirname(os.path.realpath(__file__))

config = {

	# Imago 2.0 config file


	# possibly the most important setting
	# if set to True, itll ask for a password every time the bot is started
	# this will encrypt all datasets and corpi with said password
	# that also means if you lose the password, you lose access to all user data
	# if set to False, all data will still be encrypted, but with a defined password
	# thisll still render the data unuseable if isolated, but since the password is in the source code
	# itd be trivial to decypher
	#
	# please note that switching this from false to true or from true to false after already having generated
	# some corpi or datasets that these corpi and datasets will become invalid and will have to be deleted
	# (If youre a *real* rascall you can disable secure and then change the default password in the source code ;))
	'secure': True,

	# bot prefixes
	# dont forget to add a space at the end
	'prefixes':  ['!im ','!imago '],


	# Discord status the bot will use
	# "type" can be "watching" or "playing"
	# "text" can be any text you want
	# itll display as "<type> <text>" on the bot's profile
	# in this case itd be "watching for !im"
	# set either to None if you want to disable it altogether
	'status': {
			'type': 'watching',
			'text': 'for !im help'
	},

	# time a user has to wait to execute a command again
	# times are in seconds, * operators are allowed to make conversion to minutes/hours easier
	'cooldowns': {
				'default':		10,

				'update':		5*60,
				'mimic':		2.5,
				'filter':		2.5,
				'opt in':		10*60,
				'stats':		5,
				'invite':		20,
				'info':			30
				 },

	# URL you get from discord to add the bot to other servers
	# leave empty if you dont want users to be able to get an invite link
	'invite': 'https://discord.com/api/oauth2/authorize?client_id=731258805685583882&permissions=117760&scope=bot',

	# forbidden strings in a list that the bot may not send
	# words are checked case-insensitive
	'blacklist': [
				'@everyone',
				'@here',
				'death',
				'kill',
				'abuse',
				'daughter',
				'son',
				'tranny',
				'slur',
				'trap',
				'dead',
				'nude',
				'lewd',
				 ],


	# markov settings
	# these are only "hints",
	# the generator author can choose to ignore these
	'markov': 	{
		# max state size to test against
		# *very* unlikely to ever need to be higher than 8
		'max_statesize':		6,
		# minimum corpus quality (None vs valid ratio)
		# higher = less good results, but more overal results
		'minimum_quality':		0.6,
		# how many samples to use to determine corpus quality (higher = more accurate)
		'test_samples':			100,
		# max tries, check markovify documention
		'max_tries':			100,
		# max overlap ratio, check markovify documentation
		'max_overlap_ratio':	0.8,
		# max messages per channel to use (higher = slower)
		'max_messages':			1000000
	},

	# the file in which various metadata of the users will be stored
	'user_lookup': 'users.json',
	# the folder in which the various (encrypted) user datasets are stored
	'data_folder': 'data',

	# the file the discord token should be stored in
	# token will be asked for on first launch
	# if you want to use another token, simply delete the file and restart
	'token_file': 'token.txt',


	'root': root
}


if __name__ == '__main__':
	print('Open this file in a text editor like notepad or nano to change imago settings!')
	input('press enter to exit')
