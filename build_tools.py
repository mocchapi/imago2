#!/usr/bin/python3
import time
import random
import asyncio
import datetime

import markovify

from imago_config import config

async def collectMessages(ctx,limit=None,untilTime=0,updaterange=(150,200),logger=None):
	starttime = time.time()

	# self.userTools.building[self.userTools._getsha_()]
	userID = ctx.author.id
	if limit == None:
		limit = 999999999999999999

	# print('untilTime:',untilTime)
	channels = ctx.guild.text_channels
	# authorFilter = await self.makeFilter(userID)
	if logger:
		logger.info(f'Downloading messages for {ctx.author} in {ctx.guild}')
	if untilTime == 0:
		untilTime = ctx.author.joined_at.timestamp()
	latest_timestamp = untilTime
	server_data = []
	joined_at_timestamp = ctx.author.joined_at.timestamp()
	tracker = progressUpdateHandler(ctx, channels)
	await tracker.setup()
	for channel in channels:
		counter = 0
		updatecounter = 0
		channel_data = []
		if logger:
			logger.info(f'Downloading channel {channel.name}')
		await tracker.update(channel.name,'work')
		try:
			# channel_obj = await
			async for message in channel.history(limit=limit,after=datetime.datetime.utcfromtimestamp(untilTime)):
				counter +=1
				updatecounter +=1
				if updatecounter >= random.randint(updaterange[0],updaterange[1]):
					await tracker.update(channel.name,'work',len(channel_data),counter)
					updatecounter = 0
				# filters all the channel messages for specific user before x timestamp
				try:
					if message.author.id != userID: continue
					output = str(message.content)
					if output.startswith(tuple(config['prefixes'])): continue
					if output == '': continue
					msg_timestamp = message.created_at.timestamp()
					# print(msg_timestamp, latest_timestamp,msg_timestamp > latest_timestamp)
					if msg_timestamp < untilTime: break
					if msg_timestamp < joined_at_timestamp: break
					if msg_timestamp > latest_timestamp: latest_timestamp = msg_timestamp
					for chnl in message.channel_mentions:
						output = output.replace(f'<#{chnl.id}>', '#'+str(chnl.name))
					for mention in message.mentions:
						output = output.replace(f'<@!{mention.id}>',str(mention))
						output = output.replace(f'<@{mention.id}>',str(mention))
					channel_data.append(output)
					# print(f'{message.author}: {output}')
				except Exception as e:
					logger.error(e)
					continue
			if logger:
				logger.info(f'Downloaded {len(channel_data)}/{counter} messages')
			server_data += channel_data
			# channel_data = await channel.history(limit=limit).filter(authorFilter).flatten()
		except Exception as e:
			if logger:
				logger.error(e)
			await tracker.update(channel.name,'error', msg=str(e))
			continue

		await tracker.update(channel.name,'done',len(channel_data),counter)


	await tracker.finalise()
	if logger:
		logger.info(f'done in {time.time()-starttime} with {len(server_data)} messages')
	#	def setData(self,user,data,latest_timestamp,creation_timestamp=time.time(),path=None):
	# self.userTools.setData(userID,)
	return server_data,latest_timestamp,time.time()-starttime


class progressUpdateHandler():
	def __init__(self,ctx,channels,compact=None):
		self.notes = []
		self.compact = compact
		self.space=22
		self.ctx = ctx
		self.channels = channels
		self.processed = 0
		self.counts = 0
		self.totalcounts = 0
		if self.compact == None:
			if len(self.channels) > 8:
				self.compact = True
		self.states = {
		'error': '❌',
		'work':  '🔄',
		'wait':  '🔳',
		'done':  '✅',
		}
		self.progressDict = {}
		for item in self.channels:
			self.progressDict[item.name] = {
			'state': 'wait',
			'count': 0,
			'total': 0,
			'end':   ''
			}

	async def finalise(self):
		await self.ogMsg.edit(content=await self.makeMsg())

	async def getCurrent(self):
		for item in self.progressDict:
			if self.progressDict[item].get('state') == 'work':
				return item

	async def setup(self):
		self.ogMsg = await self.ctx.send(await self.makeMsg())

	async def calctotal(self):
		count = 0
		for item in self.progressDict:
			count += int(self.progressDict.get(item,{}).get('total',0))
		return count

	async def calcusedtotal(self):
		count = 0
		for item in self.progressDict:
			count += int(self.progressDict.get(item,{}).get('count',0))
		return count

	async def update(self,name,state,count=None,total=None, msg=None):
		if msg:
			self.notes.append(f'{name}: {msg}')
		try:
			if count == None:
				count = int(self.progressDict[name]['count'])

			if total == None:
				total = int(self.progressDict[name]['total'])

			if not total and not count:
				end = ''
			else:
				end = f'{count} {" "*(3-len(str(count)))}({total})'

			if state == 'done':
				self.processed += 1
			self.progressDict[name] = {
			'state': state,
			'count': count,
			'total': total,
			'end':   end
			}
			await self.editMsg(await self.makeMsg())
		except BaseException as e:
			print('ERROR:',e)

	async def editMsg(self,msg):
		await self.ogMsg.edit(content=msg)

	async def makeMsg(self):
		if self.compact:
			out = await self.Mcompact()
		else:
			out = await self.Muncompact()
		if self.notes:
			notes = '\nNotes:```\n' + '\n'.join(self.notes) + '```'
			out += notes
		if len(out) > 2000:
			out = out[:1980]+'...```'
		return out

	async def Mcompact(self):
		msgLines = []
		msgLines.append(f'__Dataset build progress for {self.ctx.author}:__')
		msgLines.append('```')
		statusLine = ['[']
		for item in self.progressDict:
			statusLine.append(self.states[self.progressDict[item]['state']])
		statusLine.append(']')
		msgLines.append(''.join(statusLine))
		msgLines.append(' ')
		currentChnl = await self.getCurrent()
		if currentChnl:
			msgLines.append(f'Current channel: {" "*(self.space-10)}{currentChnl}')
		else:
			msgLines.append(' ')
		msgLines.append(f'Total messages: {" "*(self.space-9)}{await self.calcusedtotal()} {" "*(3-len(str(await self.calcusedtotal())))}({await self.calctotal()})')
		msgLines.append(f'Channels processed: {" "*(self.space-13)}{self.processed}/{len(self.channels)}')
		msgLines.append('```')
		out = '\n'.join(msgLines)
		if len(out) > 2000:
			out = out[:1980]+'...```'
		return out

	async def Muncompact(self):
		msgLines = []
		msgLines.append(f'__Dataset build progress for {self.ctx.author}:__')
		msgLines.append('```')
		msgLines.append(f'Channel name {" "*(self.space-12)}state msg (total msg)')
		msgLines.append(' ')
		for item in self.progressDict:
			msgLines.append(f'#{item}{" "*(self.space-len(item))} [{self.states[self.progressDict[item]["state"]]}] {self.progressDict[item]["end"]}')

		msgLines.append(' ')
		msgLines.append(f'Total messages: {" "*(self.space-9)}{await self.calcusedtotal()} {" "*(3-len(str(await self.calcusedtotal())))}({await self.calctotal()})')
		msgLines.append(f'Channels processed: {" "*(self.space-14)}[{self.processed}/{len(self.channels)}]')
		msgLines.append('```')
		out = '\n'.join(msgLines)
		if len(out) > 2000:
			out = out[:1980]+'...```'
		return out




def getRawDataset(dataset,returnAsString=True):
	out = []
	for item in dataset['servers']:
		out += dataset['servers'][item].get('raw',[])
	if returnAsString:
		out = '\n'.join(out)
	return out
